
public class VetorDinamico {
	
	private int tamanho;
	private int[] data;
	
	public VetorDinamico() {
		
		this.tamanho = 0;
		this.data = new int[0];
	}
	
	public void insereFinal(int valor) {
		this.tamanho++;
		this.aumentaTamanho();
		this.data[this.tamanho - 1] = valor;
	}
	
	public void insereInicio(int valor) {
		this.tamanho++;
		this.aumentaTamanho();
		this.moveParaFrente(0);
		this.data[0] = valor;
	}
	
	public void insere(int indice, int valor) {
		if (this.validaIndice(indice) || indice == 0) {
			this.tamanho++;
			this.aumentaTamanho();
			this.moveParaFrente(indice);
			this.data[indice] = valor;
		}else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	public void removeFinal() {
		this.tamanho--;
		this.diminuiTamanho();
	}
	
	public void removeInicio() {
		this.tamanho--;
		this.moveParaTras(0);
		this.diminuiTamanho();
	}
	
	public void remove(int indice) {
		if (this.validaIndice(indice)) {
			this.tamanho--;
			this.moveParaTras(indice);
			this.diminuiTamanho();
		}else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	public int get(int indice) {
		if (this.validaIndice(indice)) {
			return this.data[indice];
		}else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	public int size() {
		return this.tamanho;
	}
	
	public int capacity() {
		return this.data.length;
	}
	
	public boolean isEmpty() {
		return this.tamanho == 0;
	}
	
	private void moveParaTras(int indice) {
		if (this.validaIndice(indice)) {
			for (int i = indice + 1; i < data.length; i++) {
				this.data[i - 1] = this.data[i];
			}
		}
	}

	private void moveParaFrente(int indice) {
		if (this.validaIndice(indice)) {
			for (int i = data.length - 1 ; i > indice ; i--) {
				this.data[i] = this.data[i -1];
			}
		}
	}
	
	private boolean validaIndice(int indice) {
		return (indice >= 0) && (indice < this.tamanho);
	}
	
	private void aumentaTamanho() {
		if (this.tamanho > this.data.length) {
			int[] newData = new int[this.tamanho];
			
			for (int i = 0; i < this.data.length; i++) {
				newData[i] = this.data[i];
			}
			
			this.data = newData;
		}
	}
	
	private void diminuiTamanho() {
		if(this.tamanho < this.data.length) {
			int[] newData = new int[this.tamanho];
			
			for (int i = 0; i < newData.length; i++) {
				newData[i] = this.data[i];
			}
			
			this.data = newData;
		}
	}
}
