
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VetorDinamico vd = new VetorDinamico();
		
		// Retorna true pois está vazio
		if (vd.isEmpty()) {
			System.out.println("Está vazio\n");
		}else {
			System.out.println("Tem elementos\n");
		}
		
		
		// Mostra o número 20
		vd.insere(0, 20);
		System.out.println(vd.get(0)+"\n");
		
		// Mostra numero 33
		vd.insereFinal(33);
		System.out.println(vd.get(1)+"\n");
		
		// Mostra numero 17
		vd.insereInicio(17);
		System.out.println(vd.get(0)+"\n");
		
		// Mostra número 20
		vd.removeInicio();
		System.out.println(vd.get(0)+"\n");
		
		// Mostra numero 55
		vd.insere(1, 55);
		System.out.println(vd.get(1)+"\n");
		
		// Mostra a quantidade que é 3
		System.out.println(vd.size()+"\n");
		
		// Mostra a quantidade que é 2
		vd.remove(1);
		System.out.println(vd.size()+"\n");
		
		// Mostra numero 33
		System.out.println(vd.get(1)+"\n");
		
		try {
			// Gera exceção
			vd.get(2);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Gerou exceção");
		}
	}

}
